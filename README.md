# Introduction

Amigadepacker is a command-line tool tat uncompresses the following Amiga
formats:
* PowerPacker
* XPK SQSH
* MMCMP
* StoneCracker 4.04 (S404)

```man 1 amigadepacker``` for usage.

# Repository

```git clone https://gitlab.com/heikkiorsila/amigadepacker```

# Authors

* Maintainer: Heikki Orsila <heikki.orsila@iki.fi>
* SQSH decompression: Bert Jahn <jah@fh-zwickau.de>
* PowerPacker decompression and decryption: Stuart Caie <kyzer@4u.net>
* MMCMP decompression: Olivier Lapicque <olivierl@jps.net>
* StoneCracker (S404) decompression: Jouni "Spiv" Korhonen
